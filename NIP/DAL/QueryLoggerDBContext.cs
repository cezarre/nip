﻿using NIP.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NIP.DAL
{
    public class QueryLoggerDBContext : DbContext
    {
        public QueryLoggerDBContext() : base("QueryLoggerDBContext")
        {

        }

        public DbSet<QueryLogger> Logs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<QueryLoggerDBContext>());
            base.OnModelCreating(modelBuilder);
        }
    }
}