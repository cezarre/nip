﻿using NIP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NIP.DAL
{
    public class CompaniesInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<CompDBContext>
    {
        protected override void Seed(CompDBContext context)
        {
            base.Seed(context);

            var companies = new List<Company>
            {
                new Company{Name="Amex", KRS="0000000001", NIP="1111111111", Apartment_no="22", REGON="101010101", Street="Warszawska", Zip_code="12-345", City="Poznań"},
                new Company{Name="Bmex", KRS="0000000002", NIP="2222222222", Apartment_no="1", REGON="202020202", Street="Krakowska", Zip_code="42-124", City="Poznań"},
                new Company{Name="Cmex", KRS="0000000003", NIP="3333333333", Apartment_no="13", REGON="303030303", Street="Poznańska", Zip_code="92-623", City="Poznań"},
                new Company{Name="Dmex", KRS="0000000004", NIP="4444444444", Apartment_no="32", REGON="404040404", Street="Wrocławska", Zip_code="53-684", City="Poznań"}
            };
            companies.ForEach(c => context.Companies.Add(c));
            context.SaveChanges();
            
        }
    }
}