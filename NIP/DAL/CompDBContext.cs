﻿using NIP.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NIP.DAL
{
    public class CompDBContext : DbContext
    {
        public CompDBContext() : base("CompDBContext")
        {

        }

        public DbSet<Company> Companies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<CompDBContext>(null);
            base.OnModelCreating(modelBuilder);
        }

    }
}