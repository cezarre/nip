﻿using NIP.DAL;
using NIP.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NIP.Controllers
{
    public class CompanyController : Controller
    {
        private CompDBContext db = new CompDBContext();

        // GET: Company
        public ActionResult Index()
        {
            var companies = from e in db.Companies
                            orderby e.ID
                            select e;

            return View(companies);
        }
    }
}
