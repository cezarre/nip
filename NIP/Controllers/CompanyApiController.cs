﻿using NIP.DAL;
using NIP.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace NIP.Controllers
{


    public class CompanyApiController : ApiController
    {
        private CompDBContext db = new CompDBContext();

        public IHttpActionResult GetCompanyData(string comp_no)
        {
            try
            {
                System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;

                QueryLogger queryLog = new QueryLogger
                {
                    dateTime = DateTime.Now,
                    Comp_no = comp_no,
                    Connection = String.Join("; ", headers.GetValues("Connection")),
                    Accept = String.Join("; ", headers.GetValues("Accept")),
                    Accept_Encoding = String.Join("; ", headers.GetValues("Accept-Encoding")),
                    Accept_language = String.Join("; ", headers.GetValues("Accept-Language")),
                    Host = String.Join("; ", headers.GetValues("Host")),
                    Referer = String.Join("; ", headers.GetValues("Referer")),
                    User_Agent = String.Join("; ", headers.GetValues("User-Agent")),
                    X_Requested_With = String.Join("; ", headers.GetValues("X-Requested-With"))

                };
                using (QueryLoggerDBContext db_query = new QueryLoggerDBContext())
                {
                    db_query.Logs.Add(queryLog);
                    db_query.SaveChanges();
                }
            }
            catch (InvalidOperationException e)
            {
                Debug.WriteLine(e.Message);
            }
            
                
            
            //Unify NIP numbers
            comp_no = Regex.Replace(comp_no, @"[\D-]", string.Empty);
            
            var comp = (from e in db.Companies
                        where e.KRS.Equals(comp_no) ||
                              e.NIP.Equals(comp_no) ||
                              e.REGON.Equals(comp_no)
                        select e).SingleOrDefault();

            
            if (comp == null)
            {
                return NotFound();
            }
            return Ok(comp);
        }
    }
}
