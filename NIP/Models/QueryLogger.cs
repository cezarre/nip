﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NIP.Models
{
    public class QueryLogger
    {
        [Key]
        public int ID { get; set; }
        public DateTime dateTime { get; set; }
        public string Comp_no { get; set; }
        public string Connection { get; set; }
        public string Accept { get; set; }
        public string Accept_Encoding { get; set; }
        public string Accept_language { get; set; }
        public string Host { get; set; }
        public string Referer { get; set; }
        public string User_Agent { get; set; }
        public string X_Requested_With { get; set; }
    }
}