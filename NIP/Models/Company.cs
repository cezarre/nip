﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NIP.Models
{
    public class Company
    {
        public int ID { get; set; }
        public string NIP { get; set; }
        public string REGON { get; set; }
        public string KRS { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string Apartment_no { get; set; }
        public string Zip_code { get; set; }
        public string City { get; set; }
    }

}